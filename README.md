# some-note

```common-lisp
;https://github.com/spratt/cl-websocket/blob/master/cl-websocket.lisp

(defun split-string (string &key (delimiter (string #\space)) (max -1))
    "returns a list of substrings of string divided by the delimiter.  
    if there are more than max delimiters in the string, 
    the last substring will be contain the unsplit string remaining after the maxth delimiter.
    two consecutive delimiters will be seen as if there were an empty string between them."
    (let ((pos (search delimiter string)))
        (if (or (= max 0) (eq nil pos))
	        (list string)
	        (cons
	            (subseq string 0 pos)
	            (split-string (subseq string (+ pos (length delimiter)))
			        :delimiter delimiter
			        :max (if (= max -1) -1 (- max 1)))))))

(defun parse-packet (http-packet)
    "take http packet, and return a cons"
    (split-string http-packet :delimiter #(#\newline #\newline) :max 1))

(defun parse-header (header)
    "take http header and return a list of sublists of strings,
    the first string is the fieldname
    the second string is the value of that field."
    (loop for string in (split-string header :delimiter (string #\newline))
        collect (split-string string :max 1)))

(defun parse-header-list (header-list) nil) ;not yet

(defun get-field (fields fieldname)
    "take the output of parse-header and returns the value of the given fieldname."
    (dolist (field fields)
        (when (string= fieldname (subseq (car field) 0 (- (length (car field)) 1)))
            (return (cadr field)))))
```

```common-lisp
;https://github.com/sharplispers/split-sequence

(defun delimiter-char (the-char)
    "see the char as delimiter"
    (or (equal the-char #\space)  
        (equal the-cahr #\return)
        (equal the-char #\newline)))

(defun split-text (the-text the-count)
    "split the text when it has delimiter"
    (split-sequence-if #'delimiter-char the-text 
        :count the-count
        :remove-empty-subseqs t))
```

```common-lisp
;https://stackoverflow.com/questions/5457346/lisp-function-to-concatenate-a-list-of-strings

(defun append-string-a (the-list) ;7.5s
    (apply #'concatenate 'string the-list))

(defun append-string-b (the-list) ;10.2s
    (format nil "~{~A~}" the-list))

(defun append-string-c (the-list) ;10.6s
    (with-output-to-string (*standard-output*)
        (mapcar (lambda (a) (princ a)) the-list)))
```

```common-lisp
;http://clhs.lisp.se/Body/f_stg_tr.htm

(string-right-trim '(#\return #\space #\tab #\newline) 
"1 2 3 4 5 
")
```

```common-lisp
;https://stackoverflow.com/questions/1310783/how-to-read-input-until-eof-in-lisp

(loop for line = (read-line stream nil :eof)
      until (eq line :eof)
      do (print line))

(loop for line = (read-line stream nil nil)
      while line
      do (print line))
```

```common-lisp
;https://github.com/html/clache/blob/master/src/utils.lisp

(defun sequence-hex-string (the-sequence)
    (format nil "~{~2,'0X~}" (coerce the-sequence 'list)))
```

```common-lisp
;https://sodocumentation.net/common-lisp/topic/1369/loop--a-common-lisp-macro-for-iteration#looping-over-hash-tables

(loop for (k v) on the-list by #'cddr do (print (cons k v)))
```

```common-lisp
;https://github.com/cffi/cffi/blob/master/examples/examples.lisp

(defpackage #:cffi-examples
    (:use #:cl #:cffi)
    (:export #:getenv))

(in-package #:cffi-examples)

(defcfun "getenv" :string
    (name :string))

(format t "[shell]: ~A~%" (getenv "SHELL"))
```


